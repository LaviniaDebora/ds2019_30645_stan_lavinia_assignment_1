import axios from 'axios'
import AuthenticationService from "./AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();
const COURSE_API_URL = 'http://localhost:8080'

   function getAllPatients(name) {
        return axios.get('http://localhost:8080/patient/${INSTRUCTOR}/all');
    }

   function findPatientById(name, id) {
      return axios.get('http://localhost:8080/patient/${INSTRUCTOR}' + '/' + id);
    }

    function addNewPatient(name, patient) {
        return axios.post('http://localhost:8080/patient/${INSTRUCTOR}/create/', patient);
    }
    function updatePatient(name, patient) {
        return axios.put('http://localhost:8080/patient/${INSTRUCTOR}/update', patient)
    }

    function deletePatient(name, id) {
        return axios.delete('http://localhost:8080/patient/${INSTRUCTOR}/delete/'+id);
    }

    function getAllCaregivers(name) {
       return axios.get('http://localhost:8080/caregiver/${INSTRUCTOR}/all');
    }

    function findCaregiverById(name, id) {
       return axios.get('http://localhost:8080/caregiver/${INSTRUCTOR}' + '/' + id);
    }

    function addNewCaregiver(name, caregiver) {
       return axios.post('http://localhost:8080/caregiver/${INSTRUCTOR}/create/', caregiver);
    }

    function updateCaregiver(name, caregiver) {
       return axios.put('http://localhost:8080/caregiver/${INSTRUCTOR}/update', caregiver)
    }

    function deleteCaregiver(name, id) {
       return axios.delete('http://localhost:8080/caregiver/${INSTRUCTOR}/delete/' + id);
    }

    function getAllMedications(name) {
      return axios.get('http://localhost:8080/medication/${INSTRUCTOR}/all');
    }

    function findMedicationById(name, id) {
      return axios.get('http://localhost:8080/medication/${INSTRUCTOR}' + '/' + id);
    }

    function addNewMedication(name, medication) {
       return axios.post('http://localhost:8080/medication/${INSTRUCTOR}/create/', medication);
    }

    function updateMedication(name, medication) {
        return axios.put('http://localhost:8080/medication/${INSTRUCTOR}/update', medication)
    }

     function deleteMedication(name, id) {
        return axios.delete('http://localhost:8080/medication/${INSTRUCTOR}/delete/' + id);
    }

     function addNewMedicationPlan(name, medicationplan) {
         return axios.post('http://localhost:8080/medicationplan/${INSTRUCTOR}/create/', medicationplan);
   }

   function findUserByUsername(name, username) {
      return axios.get('http://localhost:8080/user/${INSTRUCTOR}' + '/' + username);
  }

export {
       getAllPatients,
        findPatientById,
        addNewPatient,
        updatePatient,
        deletePatient,
        getAllCaregivers,
        findCaregiverById,
        addNewCaregiver,
        updateCaregiver,
        deleteCaregiver,
        getAllMedications,
        findMedicationById,
        addNewMedication,
        updateMedication,
        deleteMedication,
        addNewMedicationPlan,
        findUserByUsername
}