import React, { Component } from 'react'
import "./UpdateCaregiver.css";

import {updateCaregiver,findCaregiverById} from "../../service/ApiService";
import AuthenticationService from "../../service/AuthenticationService";

const INSTRUCTOR = AuthenticationService.getLoggedInUserName();

function getValue(key){
    return localStorage.getItem(key);
}

class UpdateCaregiver extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            birthdate: null,
            gender: '',
            address: '',
            medicalRecord:[],
            toCaregiver:false,
            toCaregiverSave:false
        }
        this.updateCaregiverDetails = this.updateCaregiverDetails.bind(this);
        this.getCaregiver = this.getCaregiver.bind(this);
    }

    componentDidMount() {
        this.getCaregiver();
    }

    myChangeHandler = (e) => this.setState({ [e.target.name]: e.target.value });

    cancelUpdate=(e)=>{this.setState(() => ({toCaregiver: true}));}

    updateCaregiverDetails = (e) => {
        e.preventDefault();
        const{id, name, birthdate, gender, address, medicalRecord}=this.state;
        let caregiver = {
            id: id,
            name: name,
            birthdate: birthdate,
            gender: gender,
            address: address,
            medicalRecord:medicalRecord};

        updateCaregiver(INSTRUCTOR,caregiver).then(res => {this.setState(() => ({toCaregiverSave: true}));});
    }

    getCaregiver =()=> {
        const id = getValue("caregiverId");
        findCaregiverById(INSTRUCTOR,id).then((caregiver) => {
            this.setState({
                id: caregiver.data.id,
                name: caregiver.data.name,
                birthdate: caregiver.data.birthdate,
                gender: caregiver.data.gender,
                address: caregiver.data.address,
                medicalRecord: caregiver.data.medicalRecord,
            })
        });
    }

    render() {
        if (this.state.toCaregiver === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/all')
        }

        if (this.state.toCaregiverSave === true) {
            this.props.history.push('/caregiver/${INSTRUCTOR}/all')
        }
        return (
            <form >
                <div className="form-style-2">
                    <div className="form-style-2-heading"> Update caregiver [{this.state.id}] details</div>

                    <div>
                        <label>Name:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='name'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label>Birthdate: </label>
                        <input
                            className="input-field"
                            type='date'
                            name='birthdate'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                        <label>Gender: </label>
                        <input
                            className="input-field"
                            type='select'
                            name='gender'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <div>
                        <label >Address:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='address'
                            onChange={this.myChangeHandler}
                        />

                    </div>

                    <div>
                        <label>Medical Record:  </label>
                        <input
                            className="input-field"
                            type='text'
                            name='medicalrecord'
                            onChange={this.myChangeHandler}
                        />
                    </div>

                    <br/>
                    <br/>
                    <button  className="myButton" onClick={this.updateCaregiverDetails}> Save </button>
                    <button className="pad"  onClick={this.cancelUpdate}> Cancel </button>
                </div>
            </form>
        );
    }
}

export default UpdateCaregiver;