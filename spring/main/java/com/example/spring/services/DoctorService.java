package com.example.spring.services;

import com.example.spring.dto.builders.DoctorBuilder;
import com.example.spring.dto.builders.DoctorViewBuilder;
import com.example.spring.entities.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.errorhandler.ResourceNotFoundException;
import com.example.spring.repositories.DoctorRepository;
import com.example.spring.validators.DoctorFieldValidator;
import com.example.spring.dto.DoctorDTO;
import com.example.spring.dto.DoctorViewDTO;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public Integer insert(DoctorDTO doctorDTO) {
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);
        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctors = doctorRepository.getAllOrdered();
        return doctors.stream().map(DoctorViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public DoctorViewDTO findDoctorById(Integer id){
        Optional<Doctor> doctor  = doctorRepository.findById(id);
        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "doctor id", id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public Integer update(DoctorDTO doctorDTO) {

        Optional<Doctor> doctor= doctorRepository.findById(doctorDTO.getId());

        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor", "doctor id", doctorDTO.getId().toString());
        }
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);

        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }

    public void delete(DoctorViewDTO doctorViewDTO){
        this.doctorRepository.deleteById(doctorViewDTO.getId());
    }
}
