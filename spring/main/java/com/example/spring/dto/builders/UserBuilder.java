package com.example.spring.dto.builders;

import com.example.spring.dto.UserDTO;
import com.example.spring.entities.User;

public class UserBuilder {

    public UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User user){
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getRole());
    }

    public static User generateEntityFromDTO(UserDTO userDTO){
        return new User(
                userDTO.getId(),
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getRole());
    }
}
