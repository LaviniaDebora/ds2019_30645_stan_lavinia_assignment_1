package com.example.spring.dto;

import com.example.spring.entities.Caregiver;
import com.example.spring.entities.User;
import java.util.List;
import java.util.Objects;

public class DoctorDTO {

    private Integer id;
    private String name;
    //private List<Caregiver> caregiversList;
    private User doctorUser;

    public DoctorDTO(){
    }

    public DoctorDTO(Integer id, String name){
        this.id=id;
        this.name=name;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() { return doctorUser; }

    public void setUser(User patientCaregiver) { this.doctorUser = doctorUser; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(id, doctorDTO.id) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(doctorUser, doctorDTO.doctorUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, doctorUser);
    }
}
